package cz.vendasky;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label output;


    @FXML
    private void okButtonAction() {
        if (username.getText().equals("Login") && password.getText().equals("Password")) {
            output.setTextFill(Paint.valueOf("green"));
            output.setText("Šperka is logging...");
        } else {
            output.setTextFill(Paint.valueOf("red"));
            output.setText("Authentication error.");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
